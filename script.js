fetch('https://jsonplaceholder.typicode.com/todos')
.then(data=>data.json())
.then(data=>{
    data.forEach((element)=>{
        console.log(element)
    })
})

fetch('https://jsonplaceholder.typicode.com/todos')
.then(data=>data.json())
.then(data=>{
   data.map((element)=>{
       const title = [element.title]
       console.log(title)
   })
})

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(data=>data.json())
.then(data=>{
    console.log(`Title ${data.title}, Status ${data.completed}`)
})

fetch('https://jsonplaceholder.typicode.com/todos', {
    method: "POST",
    headers: {
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        title: "Added to do list",
        completed: false
    })
})
.then(data=>data.json())
.then(data=>{
    console.log(data)
})

fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: "PUT",
    headers: {
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        title: "Added to do list via PUT METHOD",
        completed: true
    })
})
.then(data=>data.json())
.then(data=>{
    console.log(data)
})

fetch('https://jsonplaceholder.typicode.com/todos/3', {
    method: "PATCH",
    headers: {
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        title: "Added to do list via UPDATE METHOD",
        description: "is this correct?",
        status: true,
        dateCompleted: "2022-03-08",
        userId: 1000
    })
})
.then(data=>data.json())
.then(data=>{
    console.log(data)
})

fetch('https://jsonplaceholder.typicode.com/todos/4', {
    method: "PUT",
    headers: {
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
        title: "Added desc, date, userID",
        description: "lutang na isip ko",
        status: true,
        dateCompleted: "2022-03-13",
        userId: "test"
    })
})
.then(data=>data.json())
.then(data=>{
    console.log(data)
})


fetch('https://jsonplaceholder.typicode.com/todos/200', {
    method: "DELETE"
})